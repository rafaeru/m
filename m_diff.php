<?php

/* 
Accurate time difference in days implemented in PHP.
This system uses Unix time to store the date.
To convert a date to Unix time, the following website can be used: https://www.epochconverter.com/
*/

$m_millis = 1649260500000; # 2022-04-06 16:55 WEST (UTC+1)
echo (round(microtime(true) * 1000) - $m_millis)/ (1000 * 60 * 60 * 24);
echo "\n";
?>
